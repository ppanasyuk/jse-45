package ru.t1.panasyuk.tm.service;

import io.qameta.allure.junit4.DisplayName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.panasyuk.tm.api.service.IPropertyService;
import ru.t1.panasyuk.tm.marker.UnitCategory;

@Category(UnitCategory.class)
@DisplayName("Тестирование сервиса PropertyService")
public class PropertyServiceTest {

    @NotNull
    private IPropertyService propertyService;

    @Before
    public void initService() {
        propertyService = new PropertyService();
    }

    @Test
    @DisplayName("Получение наименования файла с настройками")
    public void getApplicationConfigTest() {
        @Nullable final String value = propertyService.getApplicationConfig();
        Assert.assertNotNull(value);
        Assert.assertFalse(value.isEmpty());
    }

    @Test
    @DisplayName("Получение директории для логирования")
    public void getApplicationLog() {
        @Nullable final String value = propertyService.getApplicationLog();
        Assert.assertNotNull(value);
        Assert.assertFalse(value.isEmpty());
    }

    @Test
    @DisplayName("Получение наименования приложения")
    public void getApplicationName() {
        @Nullable final String value = propertyService.getApplicationName();
        Assert.assertNotNull(value);
        Assert.assertFalse(value.isEmpty());
    }

    @Test
    @DisplayName("Получение версии приложения")
    public void getApplicationVersion() {
        @Nullable final String value = propertyService.getApplicationVersion();
        Assert.assertNotNull(value);
        Assert.assertFalse(value.isEmpty());
    }

    @Test
    @DisplayName("Получение Email автора")
    public void getAuthorEmail() {
        @Nullable final String value = propertyService.getAuthorEmail();
        Assert.assertNotNull(value);
        Assert.assertFalse(value.isEmpty());
    }

    @Test
    @DisplayName("Получение имени автора")
    public void getAuthorName() {
        @Nullable final String value = propertyService.getAuthorName();
        Assert.assertNotNull(value);
        Assert.assertFalse(value.isEmpty());
    }

    @Test
    @DisplayName("Получение ветки коммита Git")
    public void getGitBranch() {
        @Nullable final String value = propertyService.getGitBranch();
        Assert.assertNotNull(value);
        Assert.assertFalse(value.isEmpty());
    }

    @Test
    @DisplayName("Получение Id коммита Git")
    public void getGitCommitId() {
        @Nullable final String value = propertyService.getGitCommitId();
        Assert.assertNotNull(value);
        Assert.assertFalse(value.isEmpty());
    }

    @Test
    @DisplayName("Получение пользователя выполнившего коммит Git")
    public void getGitCommitterName() {
        @Nullable final String value = propertyService.getGitCommitterName();
        Assert.assertNotNull(value);
        Assert.assertFalse(value.isEmpty());
    }

    @Test
    @DisplayName("Получение Email пользователя выполневшего коммит Git")
    public void getGitCommitterEmail() {
        @Nullable final String value = propertyService.getGitCommitterEmail();
        Assert.assertNotNull(value);
        Assert.assertFalse(value.isEmpty());
    }

    @Test
    @DisplayName("Получение сообщения коммита Git")
    public void getGitCommitMessage() {
        @Nullable final String value = propertyService.getGitCommitMessage();
        Assert.assertNotNull(value);
        Assert.assertFalse(value.isEmpty());
    }

    @Test
    @DisplayName("Получение времени коммита Git")
    public void getGitCommitTime() {
        @Nullable final String value = propertyService.getGitCommitTime();
        Assert.assertNotNull(value);
        Assert.assertFalse(value.isEmpty());
    }

    @Test
    @DisplayName("Получение порта сервера")
    public void getServerPort() {
        @Nullable final String value = propertyService.getServerPort();
        Assert.assertNotNull(value);
        Assert.assertFalse(value.isEmpty());
    }

    @Test
    @DisplayName("Получение хоста сервера")
    public void getServerHost() {
        @Nullable final String value = propertyService.getServerHost();
        Assert.assertNotNull(value);
        Assert.assertFalse(value.isEmpty());
    }

    @Test
    @DisplayName("Получение ключа сессии")
    public void getSessionKey() {
        @Nullable final String value = propertyService.getSessionKey();
        Assert.assertNotNull(value);
        Assert.assertFalse(value.isEmpty());
    }

    @Test
    @DisplayName("Получение таймаута сессии")
    public void getTimeout() {
        @Nullable final Integer value = propertyService.getSessionTimeout();
        Assert.assertNotNull(value);
    }

}