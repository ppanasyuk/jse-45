package ru.t1.panasyuk.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.dto.request.project.ProjectRemoveByIdRequest;
import ru.t1.panasyuk.tm.dto.response.project.ProjectRemoveByIdResponse;
import ru.t1.panasyuk.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @NotNull
    private static final String DESCRIPTION = "Remove project by id.";

    @NotNull
    private static final String NAME = "project-remove-by-id";

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(getToken(), id);
        @NotNull final ProjectRemoveByIdResponse response = getProjectEndpoint().removeProjectById(request);
        if (!response.getSuccess()) System.out.println(response.getMessage());
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}