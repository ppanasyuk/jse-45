package ru.t1.panasyuk.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.panasyuk.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.panasyuk.tm.api.service.IConnectionService;
import ru.t1.panasyuk.tm.api.service.dto.IProjectTaskDtoService;
import ru.t1.panasyuk.tm.exception.entity.ProjectNotFoundException;
import ru.t1.panasyuk.tm.exception.entity.TaskNotFoundException;
import ru.t1.panasyuk.tm.exception.field.IndexIncorrectException;
import ru.t1.panasyuk.tm.exception.field.ProjectIdEmptyException;
import ru.t1.panasyuk.tm.exception.field.TaskIdEmptyException;
import ru.t1.panasyuk.tm.dto.model.ProjectDTO;
import ru.t1.panasyuk.tm.dto.model.TaskDTO;
import ru.t1.panasyuk.tm.repository.dto.ProjectDtoRepository;
import ru.t1.panasyuk.tm.repository.dto.TaskDtoRepository;

import javax.persistence.EntityManager;
import java.util.List;

public class ProjectTaskDtoService implements IProjectTaskDtoService {

    @NotNull
    final IConnectionService connectionService;

    public ProjectTaskDtoService(
            @NotNull final IConnectionService connectionService
    ) {
        this.connectionService = connectionService;
    }

    @NotNull
    @Override
    public TaskDTO bindTaskToProject(
            @NotNull final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final TaskDTO task;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            boolean isExist = projectRepository.findOneById(userId, projectId) != null;
            if (!isExist) throw new ProjectNotFoundException();
            task = taskRepository.findOneById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProjectId(projectId);
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Override
    public ProjectDTO removeProjectById(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @Nullable ProjectDTO project;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            project = projectRepository.findOneById(userId, projectId);
            if (project == null) throw new ProjectNotFoundException();
            taskRepository.removeAllByProjectId(userId, projectId);
            projectRepository.remove(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @Override
    public ProjectDTO removeProjectByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        @Nullable final ProjectDTO project;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            if (index > projectRepository.getSize(userId)) throw new IndexIncorrectException();
            project = projectRepository.findOneByIndex(userId, index);
            if (project == null) throw new ProjectNotFoundException();
            removeProjectById(userId, project.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        return project;
    }

    @Override
    public void clearProjects(@NotNull final String userId) {
        @Nullable final List<ProjectDTO> projects;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            projects = projectRepository.findAll(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        if (projects == null) return;
        for (@NotNull final ProjectDTO project : projects) removeProjectById(userId, project.getId());
    }

    @NotNull
    @Override
    public TaskDTO unbindTaskFromProject(
            @NotNull final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final TaskDTO task;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            boolean isExist = projectRepository.findOneById(userId, projectId) != null;
            if (!isExist) throw new ProjectNotFoundException();
            task = taskRepository.findOneById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProjectId(null);
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        return task;
    }

}