package ru.t1.panasyuk.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.api.repository.model.ITaskRepository;
import ru.t1.panasyuk.tm.api.repository.model.IUserRepository;
import ru.t1.panasyuk.tm.api.service.IConnectionService;
import ru.t1.panasyuk.tm.api.service.model.ITaskService;
import ru.t1.panasyuk.tm.enumerated.Sort;
import ru.t1.panasyuk.tm.enumerated.Status;
import ru.t1.panasyuk.tm.exception.entity.TaskNotFoundException;
import ru.t1.panasyuk.tm.exception.field.*;
import ru.t1.panasyuk.tm.model.Task;
import ru.t1.panasyuk.tm.model.User;
import ru.t1.panasyuk.tm.repository.model.TaskRepository;
import ru.t1.panasyuk.tm.repository.model.UserRepository;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class TaskService extends AbstractUserOwnedService<Task, ITaskRepository>
        implements ITaskService {

    public TaskService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected ITaskRepository getRepository(@NotNull EntityManager entityManager) {
        return new TaskRepository(entityManager);
    }

    @NotNull
    private IUserRepository getUserRepository(@NotNull final EntityManager entityManager) {
        return new UserRepository(entityManager);
    }

    @NotNull
    @Override
    public Task changeTaskStatusById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusIncorrectException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        update(task);
        return task;
    }

    @NotNull
    @Override
    public Task changeTaskStatusByIndex(
            @NotNull final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (index == null || index <= 0) throw new IndexIncorrectException();
        if (index > getSize(userId)) throw new IndexIncorrectException();
        if (status == null) throw new StatusIncorrectException();
        @Nullable final Task task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        update(task);
        return task;
    }

    @NotNull
    @Override
    public Task create(
            @NotNull final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final Task task = new Task();
        try {
            @NotNull final IUserRepository userRepository = getUserRepository(entityManager);
            entityManager.getTransaction().begin();
            @Nullable final User user = userRepository.findOneById(userId);
            task.setName(name);
            task.setDescription(description);
            task.setUser(user);
            add(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @NotNull
    @Override
    public Task create(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final Task task = new Task();
        try {
            @NotNull final IUserRepository userRepository = getUserRepository(entityManager);
            entityManager.getTransaction().begin();
            @Nullable final User user = userRepository.findOneById(userId);
            task.setName(name);
            task.setUser(user);
            add(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Nullable
    @Override
    public List<Task> findAll(@NotNull final String userId, @Nullable final Comparator comparator) {
        @Nullable List<Task> models;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            models = repository.findAll(userId, comparator);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
        return models;
    }

    @Nullable
    @Override
    @SuppressWarnings("unchecked")
    public List<Task> findAll(@NotNull final String userId, @Nullable final Sort sort) {
        if (sort == null) return findAll(userId);
        final Comparator<Task> comparator = sort.getComparator();
        return findAll(userId, comparator);
    }

    @Nullable
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        @Nullable final List<Task> tasks;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            tasks = repository.findAllByProjectId(userId, projectId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
        return tasks;
    }

    @Override
    public void removeAllByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeAllByProjectId(userId, projectId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Task updateById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        update(task);
        return task;
    }

    @NotNull
    @Override
    public Task updateByIndex(
            @NotNull final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (index == null || index <= 0) throw new IndexIncorrectException();
        if (index > getSize(userId)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final Task task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        update(task);
        return task;
    }

}