package ru.t1.panasyuk.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.dto.model.AbstractUserOwnedModelDTO;

import java.util.Collection;
import java.util.List;

public interface IUserOwnedDtoService<M extends AbstractUserOwnedModelDTO> {

    M add(@NotNull String userId, @Nullable M model);

    void clear(@NotNull String userId);

    void clear();

    boolean existsById(@NotNull String userId, @Nullable String id);

    List<M> findAll();

    @Nullable
    List<M> findAll(@NotNull String userId);

    @Nullable
    M findOneById(@NotNull String userId, @Nullable String id);

    @Nullable
    M findOneByIndex(@NotNull String userId, @Nullable Integer index);

    int getSize(@NotNull String userId);

    @Nullable
    M removeById(@NotNull String userId, @Nullable String id);

    @Nullable
    M removeByIndex(@NotNull String userId, @Nullable Integer index);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    void update(@NotNull M model);

}