package ru.t1.panasyuk.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.panasyuk.tm.dto.model.SessionDTO;

import javax.persistence.EntityManager;

public final class SessionDtoRepository extends AbstractUserOwnedDtoRepository<SessionDTO> implements ISessionDtoRepository {

    @NotNull
    @Override
    protected Class<SessionDTO> getEntityClass() {
        return SessionDTO.class;
    }

    public SessionDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

}