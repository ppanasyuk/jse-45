package ru.t1.panasyuk.tm.repository.model;

import io.qameta.allure.junit4.DisplayName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.panasyuk.tm.api.repository.model.IProjectRepository;
import ru.t1.panasyuk.tm.api.repository.model.IUserRepository;
import ru.t1.panasyuk.tm.api.service.IConnectionService;
import ru.t1.panasyuk.tm.api.service.IPropertyService;
import ru.t1.panasyuk.tm.enumerated.Sort;
import ru.t1.panasyuk.tm.model.Project;
import ru.t1.panasyuk.tm.model.User;
import ru.t1.panasyuk.tm.service.ConnectionService;
import ru.t1.panasyuk.tm.service.PropertyService;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Ignore
@DisplayName("Тестирование репозитория проектов на графах")
public class ProjectRepositoryTest {

    private final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private User testUser1;

    @NotNull
    private User testUser2;

    @NotNull
    private List<Project> projectList;

    @NotNull
    private IProjectRepository projectRepository;

    @NotNull
    private static IConnectionService connectionService;

    @NotNull
    private static EntityManager entityManager;

    @BeforeClass
    public static void initConnection() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
    }

    @Before
    public void initRepository() {
        projectList = new ArrayList<>();
        entityManager = connectionService.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        projectRepository = new ProjectRepository(entityManager);
        testUser1 = new User();
        testUser2 = new User();
        userRepository.add(testUser1);
        userRepository.add(testUser2);
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName("Project " + i);
            project.setDescription("Description " + i);
            if (i < 5) project.setUser(testUser1);
            else project.setUser(testUser2);
            projectList.add(project);
            projectRepository.add(project);
        }
    }

    @After
    public void afterTest() {
        entityManager.getTransaction().rollback();
        entityManager.close();
    }


    @Test
    @DisplayName("Добавление проекта")
    public void addTest() {
        int expectedNumberOfEntries = projectRepository.getSize() + 1;
        @NotNull final String projectName = "Project Name";
        @NotNull final String projectDescription = "Project Description";
        @NotNull final Project project = new Project();
        project.setName(projectName);
        project.setDescription(projectDescription);
        project.setUser(testUser1);
        @Nullable final Project createdProject = projectRepository.add(project);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
        Assert.assertNotNull(createdProject);
        Assert.assertEquals(testUser1, createdProject.getUser());
        Assert.assertEquals(projectName, createdProject.getName());
        Assert.assertEquals(projectDescription, createdProject.getDescription());
    }

    @Test
    @DisplayName("Добавление списока проектов")
    public void addAllTest() {
        int expectedNumberOfEntries = projectRepository.getSize() + 2;
        @NotNull final List<Project> projects = new ArrayList<>();
        @NotNull final String firstProjectName = "First Project Name";
        @NotNull final String firstProjectDescription = "Project Description";
        @NotNull final Project firstProject = new Project();
        firstProject.setName(firstProjectName);
        firstProject.setDescription(firstProjectDescription);
        firstProject.setUser(testUser1);
        projects.add(firstProject);
        @NotNull final String secondProjectName = "Second Project Name";
        @NotNull final String secondProjectDescription = "Project Description";
        @NotNull final Project secondProject = new Project();
        secondProject.setName(secondProjectName);
        secondProject.setDescription(secondProjectDescription);
        secondProject.setUser(testUser1);
        projects.add(secondProject);
        @NotNull final Collection<Project> addedProjects = projectRepository.add(projects);
        Assert.assertTrue(addedProjects.size() > 0);
        int actualNumberOfEntries = projectRepository.getSize();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
    }

    @Test
    @DisplayName("Удаление проектов для пользователя")
    public void clearForUserTest() {
        int expectedNumberOfEntries = 0;
        projectRepository.clear(testUser1.getId());
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize(testUser1.getId()));
    }

    @Test
    @DisplayName("Поиск всех проектов")
    public void findAllTest() {
        int allProjectsSize = projectRepository.getSize();
        @Nullable final List<Project> projects = projectRepository.findAll();
        Assert.assertNotNull(projects);
        Assert.assertEquals(allProjectsSize, projects.size());
    }

    @Test
    @DisplayName("Поиск всех проектов по компаратору")
    public void findAllWithComparatorTest() {
        @NotNull Comparator<Project> comparator = Sort.BY_NAME.getComparator();
        int allProjectsSize = projectRepository.findAll().size();
        @NotNull List<Project> projects = projectRepository.findAll(comparator);
        Assert.assertEquals(allProjectsSize, projects.size());
        comparator = Sort.BY_CREATED.getComparator();
        projects = projectRepository.findAll(comparator);
        Assert.assertEquals(allProjectsSize, projects.size());
        comparator = Sort.BY_STATUS.getComparator();
        projects = projectRepository.findAll(comparator);
        Assert.assertEquals(allProjectsSize, projects.size());
    }

    @Test
    @DisplayName("Поиск всех проектов для пользователя")
    public void findAllForUserTest() {
        @NotNull List<Project> projectListForUser = projectList
                .stream()
                .filter(m -> testUser1.equals(m.getUser()))
                .collect(Collectors.toList());
        @Nullable final List<Project> projects = projectRepository.findAll(testUser1.getId());
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectListForUser.size(), projects.size());
    }

    @Test
    @DisplayName("Поиск всех проектов по компаратору для пользователя")
    public void findAllWithComparatorForUser() {
        @NotNull final List<Project> projectListForUser = projectList
                .stream()
                .filter(m -> testUser1.equals(m.getUser()))
                .collect(Collectors.toList());
        @NotNull Comparator<Project> comparator = Sort.BY_NAME.getComparator();
        @Nullable List<Project> projects = projectRepository.findAll(testUser1.getId(), comparator);
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectListForUser.size(), projects.size());
        comparator = Sort.BY_CREATED.getComparator();
        projects = projectRepository.findAll(testUser1.getId(), comparator);
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectListForUser.size(), projects.size());
        comparator = Sort.BY_STATUS.getComparator();
        projects = projectRepository.findAll(testUser1.getId(), comparator);
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectListForUser.size(), projects.size());
    }

    @Test
    @DisplayName("Поиск проекта по Id")
    public void findOneByIdTest() {
        @Nullable Project project;
        for (int i = 1; i <= projectList.size(); i++) {
            project = projectList.get(i - 1);
            Assert.assertNotNull(project);
            @NotNull final String projectId = project.getId();
            @Nullable final Project foundProject = projectRepository.findOneById(projectId);
            Assert.assertNotNull(foundProject);
        }
    }

    @Test
    @DisplayName("Поиск проекта по Null Id")
    public void findOneByIdNullTest() {
        @Nullable final Project foundProject = projectRepository.findOneById("qwerty");
        Assert.assertNull(foundProject);
        @Nullable final Project foundProjectNull = projectRepository.findOneById(null);
        Assert.assertNull(foundProjectNull);
    }

    @Test
    @DisplayName("Поиск проекта по Id для пользователя")
    public void findOneByIdForUserTest() {
        @NotNull List<Project> projectListForUser = projectList
                .stream()
                .filter(m -> testUser1.equals(m.getUser()))
                .collect(Collectors.toList());
        for (@NotNull final Project project : projectListForUser) {
            Assert.assertNotNull(project);
            @NotNull final String projectId = project.getId();
            @Nullable final Project foundProject = projectRepository.findOneById(testUser1.getId(), projectId);
            Assert.assertNotNull(foundProject);
        }
    }

    @Test
    @DisplayName("Поиск проекта по Null Id для пользователя")
    public void findOneByIdNullForUserTest() {
        @Nullable final Project foundProject = projectRepository.findOneById(testUser1.getId(), "qwerty");
        Assert.assertNull(foundProject);
        @Nullable final Project foundProjectNull = projectRepository.findOneById(testUser1.getId(), null);
        Assert.assertNull(foundProjectNull);
    }

    @Test
    @DisplayName("Поиск проекта по индексу")
    public void findOneByIndexTest() {
        for (int i = 1; i <= projectList.size(); i++) {
            @Nullable final Project project = projectRepository.findOneByIndex(i);
            Assert.assertNotNull(project);
        }
    }

    @Test
    @DisplayName("Поиск проекта по Null индексу")
    public void findOneByIndexNullTest() {
        @Nullable final Project project = projectRepository.findOneByIndex(null);
        Assert.assertNull(project);
    }

    @Test
    @DisplayName("Поиск проекта по индексу для пользователя")
    public void findOneByIndexForUserTest() {
        @NotNull List<Project> projectListForUser = projectList
                .stream()
                .filter(m -> testUser1.equals(m.getUser()))
                .collect(Collectors.toList());
        for (int i = 1; i <= projectListForUser.size(); i++) {
            @Nullable final Project project = projectRepository.findOneByIndex(testUser1.getId(), i);
            Assert.assertNotNull(project);
        }
    }

    @Test
    @DisplayName("Поиск проекта по Null индексу для пользователя")
    public void findOneByIndexNullForUserText() {
        @Nullable final Project project = projectRepository.findOneByIndex(testUser1.getId(), null);
        Assert.assertNull(project);
    }

    @Test
    @DisplayName("Получить количество проектов")
    public void getSizeTest() {
        int actualSize = projectRepository.getSize();
        Assert.assertTrue(actualSize > 0);
    }

    @Test
    @DisplayName("Получить количество проектов для пользователя")
    public void getSizeForUserTest() {
        int expectedSize = (int) projectList
                .stream()
                .filter(m -> testUser1.equals(m.getUser()))
                .count();
        int actualSize = projectRepository.getSize(testUser1.getId());
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    @DisplayName("Удалить проект")
    public void removeTest() throws Exception {
        @Nullable final Project project = projectList.get(0);
        Assert.assertNotNull(project);
        @NotNull final String projectId = project.getId();
        @Nullable final Project deletedProject = projectRepository.remove(project);
        Assert.assertNotNull(deletedProject);
        @Nullable final Project deletedProjectInRepository = projectRepository.findOneById(projectId);
        Assert.assertNull(deletedProjectInRepository);
    }

    @Test
    @DisplayName("Удалить Null проект")
    public void removeNullTest() {
        @Nullable final Project project = projectRepository.remove(null);
        Assert.assertNull(project);
    }

}