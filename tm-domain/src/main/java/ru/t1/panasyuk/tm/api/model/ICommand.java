package ru.t1.panasyuk.tm.api.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.enumerated.Role;

public interface ICommand {

    void execute();

    @Nullable
    String getArgument();

    @Nullable
    String getDescription();

    @NotNull
    String getName();

    @Nullable
    Role[] getRoles();

    @NotNull
    String getUserId();

}