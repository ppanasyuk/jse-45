package ru.t1.panasyuk.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectCompleteByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    public ProjectCompleteByIndexRequest(@Nullable final Integer index) {
        this.index = index;
    }

    public ProjectCompleteByIndexRequest(@Nullable final String token, @Nullable final Integer index) {
        super(token);
        this.index = index;
    }

}