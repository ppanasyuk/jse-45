package ru.t1.panasyuk.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.dto.request.AbstractUserRequest;
import ru.t1.panasyuk.tm.enumerated.Sort;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectListRequest extends AbstractUserRequest {

    @Nullable
    private Sort sort;

    public ProjectListRequest(@Nullable final Sort sort) {
        this.sort = sort;
    }

    public ProjectListRequest(@Nullable final String token, @Nullable final Sort sort) {
        super(token);
        this.sort = sort;
    }

}