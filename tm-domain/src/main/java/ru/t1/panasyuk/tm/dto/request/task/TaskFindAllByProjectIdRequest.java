package ru.t1.panasyuk.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class TaskFindAllByProjectIdRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    public TaskFindAllByProjectIdRequest(@Nullable final String projectId) {
        this.projectId = projectId;
    }

    public TaskFindAllByProjectIdRequest(@Nullable final String token, @Nullable final String projectId) {
        super(token);
        this.projectId = projectId;
    }

}