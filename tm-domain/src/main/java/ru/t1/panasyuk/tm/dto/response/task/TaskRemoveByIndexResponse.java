package ru.t1.panasyuk.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.dto.model.TaskDTO;

@Getter
@Setter
@NoArgsConstructor
public final class TaskRemoveByIndexResponse extends AbstractTaskResponse {

    @Nullable
    private TaskDTO task;

    public TaskRemoveByIndexResponse(@Nullable final TaskDTO task) {
        this.task = task;
    }

    public TaskRemoveByIndexResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}