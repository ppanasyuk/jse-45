package ru.t1.panasyuk.tm.dto.response.system;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.panasyuk.tm.dto.response.AbstractResponse;

@Getter
@Setter
@NoArgsConstructor
public final class ServerAboutResponse extends AbstractResponse {

    private String email;

    private String name;

    private String applicationName;

    private String gitBranch;

    private String gitCommitId;

    private String gitCommitterName;

    private String gitCommitterEmail;

    private String gitCommitMessage;

    private String gitCommitTime;

}